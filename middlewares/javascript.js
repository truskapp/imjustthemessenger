/**
* Sets the response as javascript
*/
module.exports = function() {
    return function(req, res, next) {
        res.setHeader('content-type', 'application/javascript; charset=utf-8');
        next();
    }
};
