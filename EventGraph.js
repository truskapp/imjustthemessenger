/*
 * GPLv3
 * Auteur Jonathan Asquier
 *
 * Le plus long chemin d'un graphe acyclique orienté non pondéré
*/

var EventGraph = function(vertices){
    this.vertices = vertices;
    this.validateNodeArray();
    if(this.valid)
        this.makeArks();
};

/*
* Test que le tableau de vertices est valide
*/
EventGraph.prototype.validateNodeArray = function() {
    this.valid = true;
    for(var k = 0; k < this.vertices.length; k++)
    {
        if(!(typeof this.vertices[k] === 'object'))
            this.valid = false;
        else if(!this.vertices[k].start || !(typeof this.vertices[k].start === 'number'))
            this.valid = false;
        else if(!this.vertices[k].end || !(typeof this.vertices[k].end === 'number'))
            this.valid = false;
        else if(this.vertices[k].end <= this.vertices[k].start)
            this.valid = false;
    }
};

/*
* Renvoi un graphe (tableau d'arcs) à partir du tableau de noeuds. Un arc est créé si deux noeuds peuvent se succéder.
* Un arc est un objet contenant l'indice du sommet de départ et l'indice du sommet d'arrivée.
*/
EventGraph.prototype.makeArks = function() {
    if(!this.valid)
        return;
    this.arks = new Array;
    for(var k = 0; k < this.vertices.length; k++)
        for(var i = 0; i < this.vertices.length; i++)
            if(k !== i)
                if(this.vertices[k].end < this.vertices[i].start)
                    this.arks.push({
                        start : k,
                        end : i
                    });
    this.initRank();
    this.makeRanks();
};

/*
* Initialise les ranks des noeuds qui n'ont pas de prédécesseurs à 0.
*/
EventGraph.prototype.initRank = function() {
    if(!this.valid)
        return;
    var notStartingVertices = new Array;
    for(var k = 0; k < this.arks.length; k++)
        if(notStartingVertices.indexOf(this.arks[k].end) == -1)
            notStartingVertices.push(this.arks[k].end);

    for(var k = 0; k < this.vertices.length; k++)
        this.vertices[k].rank = (notStartingVertices.indexOf(k) == -1) ? 0 : null;
}

/*
* Détermine le rank de chaque noeud
*/
EventGraph.prototype.makeRanks = function(vertices, arks) {
    if(!this.valid)
        return;
    var currentRank = 0;
    /*
    * Détermine si il reste des noeuds qui n'ont pas de ranks
    */
    var stillUndefinedRanks = function(v){
        x = false;
        for(var i = 0; i < v.length; i++)
            if(v[i].rank == null)
                x = true;
        return x;
    };

    while(stillUndefinedRanks(this.vertices))
    {
        for(var k = 0; k < this.vertices.length; k++)
            if(this.vertices[k].rank == null)
            {
                var t = true;
                for(var i = 0; i < this.arks.length; i++)
                    if(this.arks[i].end == k)
                        if(this.vertices[this.arks[i].start].rank > currentRank)
                            t = false;
                if(t)
                    this.vertices[k].rank = currentRank + 1;
            }
        currentRank++;
    }
};

/*
* Détermine le nombre maximal de noeuds qui peuvent se succéder
*/
EventGraph.prototype.maxEvents = function(vertices) {
    if(!this.valid)
        return;
    var maxrank = 0;
    for(var k = 0; k < this.vertices.length; k++)
        if(this.vertices[k].rank > maxrank)
            maxrank = this.vertices[k].rank;
    return maxrank + 1;
};

/*
* TODO
* Détermine la ou les routes les plus longues
*/
EventGraph.prototype.longestPaths = function() {
    if(!this.valid)
        return;
    /*
    Idée : partir de chacun des noeuds de rang maximal n, et remonter au noeud n-1 jusqu'à 0
        - gérer lorsqu'il y a plus d'un noeud de rang n
        - gérer les ronds points (2..* paths de longueur n qui partagent au moins leurs sommets de rang 0 et n)
    type de retour : un tableau de 1..* routes (tableau d'arcs) de longueur n
    */
};

module.exports = EventGraph;
