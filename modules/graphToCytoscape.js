module.exports = function(graph){
    var elements = {
        nodes : [],
        edges : []
    };
    for(var k = 0; k < graph.vertices.length; k++){
        elements.nodes.push({
            data : {
                id : 'n' + k.toString()
            }
        });
    }
    for(var k = 0; k < graph.arks.length; k++){
        elements.edges.push({
            data : {
                id : 'e' + k.toString(),
                weight : 1,
                source : 'n' + graph.arks[k].start.toString(),
                target : 'n' + graph.arks[k].end.toString()
            }
        });
    }
    return elements;
};
