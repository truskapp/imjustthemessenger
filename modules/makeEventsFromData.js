var moment = require('moment');

var makeEvent = function(startDate, endDate){
    return {
        start : moment(startDate).unix(),
        end : moment(endDate).unix()
    };
};

module.exports = function(data){
    var events = [];
    for(var k = 0; k < data.length; k++)
        events.push(makeEvent(data[k][0], data[k][1]));
    return events;
};
