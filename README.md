# I'm Just The Messenger #

## Problématique ##

Un client souhaite une course Trusk immédiatement ou plus tard, d'un lieu de départ à un lieu d'arrivée, pour déplacer des objets volumineux et plus ou moins difficiles à déplacer (1..\*).

Trusk doit assigner un trusker à la course de manière à servir le client dans les temps.
Le challenge étant de répartir équitablement aux truskers les courses tout en évitant un maximum de rejet de course et donc de redispatch.

## Contraintes ##

* Le trusker doit être en mesure de déplacer les objets de la course :
    * Avoir un camion assez volumineux
    * Avoir un assistant trusker disponible si besoin
    * Avoir le matériel nécessaire pour mener à bien la course (camion avec hayon, monte charge...)
* Le trusker doit être disponible
* Le trusker doit parcourir un minimum de distance entre son point de chute (adresse de résidence, adresse de l'entreprise...) et ses différentes courses

## Objectif ##

### v1 ###

Le but est de créer un algorithme qui attribue des scores d'éligibilité au trusker en fonction d'une course donnée, 0 étant un trusker non éligible.
L'algorithme doit :

* Filtrer les truskers disponibles en fonction de leurs caractéristiques (déménageur, trusker spécialisé dans l'art, trusker expérimenté, trusker novice...)
* Estimer la position du trusker en fonction de la date de la course (maintenant en temps réel, plus tard en exercice (destination de la dernière course), ou son point de chute s'il n'a pas de course)
* Déterminer si l'emploi du temps du trusker et la distance qui le sépare(ra) du lieu de prise en charge de la course lui permet(tra) de la prendre.
* Pondérer le score en fonction du score de satisfation du trusker, de son score de ponctualité...

L'algorithme doit être capable de (re)distribuer une course à chaque instant de manière optimisée.

### v2 ###

Trusk at scale.

#### Planifié (Methode Nestor) ####

La demande planifiée permet de remplir les semaines à l'avance. Plutôt que de proposer les courses aux truskers lorsqu'elles sont disponibles, ce sont des plannings optimisés pour réduire la distance entre chaque course qui sont distribués. Le dispatch est fait en amont, basé sur un agenda de disponibilités des truskers. (voyageur de commerce =~ auto-affect d'onfleet)

#### On demand (Methode Tok Tok Tok) ####

Le machine learning permet de déterminer des slots pour les truskers.
Un slot :

* Un créneau de temps
* Une zone géographique
* Un volume de course prévisionnel
* Un nombre de truskers
* Une prix horaire assuré (€/h)

Les slots sont déterminés en fonction du volume de courses prévisionnel en un lieu et temps donné. Ils permettent de rentre disponible des truskers dans ces zones afin qu'ils puissent répondre à la demande instantanée dans les délais les plus brefs. Les truskers sont assurés de toucher au minimum le montant du prix horaire si aucune course ne leur est distribuée (l'avantage est de pouvoir ouvrir facilement des villes on-demand, en simulant la demande grâce aux slots).

## Technologies ##

* Javascript CommonJS ES5/6 (NodeJS 4.\*.\* est l'implémentation CommonJS que nous utiliserons)

## Installation ##

Ce projet présente un graph d'événements pouvant se succéder, et de calculer le nombre d'événements maximum auxquels on peut assister à la suite.
Le but à terme est de déterminer si une course peut être intercallée dans un chemin d'événements ou pas.

Installer NodeJS https://nodejs.org/en/

Installer `pm2` :

    npm install pm2 -g

Une fois dans le projet pour installer les dépendances :

    npm install

Lancer le serveur avec pm2 :

    pm2 start processes.json

Lancer la console de debug :

    pm2 flush && pm2 logs

Ouvrir `http://localhost:3421` pour visualiser le graph

Données du graph : `/data.js` (un tableau d'évenements, chaque événement est un tableau de 2 dates: une de début, une de fin)

Voir la route `/script.js` dans le fichier `server.js` pour écrire dans la console de debug

## Recherches complémentaires ##

* KKN/Diagrammes de Voronoi (Trouver le trusker le plus proche d'une course)
* Voyageur de commerce
* PERT
