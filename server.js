var config = require('./config');
var express = require('express');
var bodyparser = require('body-parser');
var _ = require('lodash');
var javascript = require('./middlewares/javascript');
var graphToCytoscape = require('./modules/graphToCytoscape');
var ROOT = __dirname;

var app = express();
var server = require('http').Server(app);

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.set('view engine', 'jade');

// Start server
server.listen(config.server_port, function () {
    console.log('Server listening at %s', config.server_port);
});

/**
User REST authenticate
*/
app.get('/',
    function (req, res) {
        res.render('index');
    }
);

app.get('/script.js',
    javascript(),
    function(req, res) {
        /*
        * Graph
        */
        var EventGraph = require('./EventGraph');
        var makeEventsFromData = require('./modules/makeEventsFromData');
        var data = require('./data');
        var graph = new EventGraph(makeEventsFromData(data));

        /*
        * Debug
        */
        console.log(graph);
        console.log('Max events : ', graph.maxEvents());

        /*
        * Rendu graphique
        */
        res.status(20).send('var graph = ' + JSON.stringify(graphToCytoscape(graph)));
    }
);
